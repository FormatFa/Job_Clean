package bigdata.jobclean
import java.io.{File, PrintWriter}
import java.util


import org.apache.spark.sql.{Encoders, Row, RowFactory, SparkSession}
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.{DataTypes, StructField, StructType}
import org.apdplat.word.WordSegmenter

class WordCount {

}
/*
统计各个职业的技能要求词频，单个职业测试用，龟速
 */
object WordCount
{


  def main(args: Array[String]): Unit = {
//    简单粗暴版
    val line = "1. 负责产品架构设计和开发；2. 负责项目和产品跟踪与完善；3. 负责项目团队下级（初、中级）工程师的开发指导；任职要求：1.计算机相关专业，熟练掌握软件编程知识、软件工程、数据结构、数据库等基础理论；2.精通主流开发框架(SpringMVC、Springboot、SpringCloud、Mybatis，nutz）等，能根据产品进行架构的设计重构；3.熟练掌握常用的开发工具和技术（Maven、Git、Svn等）；4.掌握sql server  ，oracle ，mysql等关系型数据库等关系型数据，能够独立完成数据库的设计和开发工作。5.团队意识强、沟通能力好、责任心重、对技术有强烈的兴趣和追求；6.对于医疗行业有经验者优先考虑。职能类别：高级软件工程师软件工程师关键字：Java开发高级微信分享"

    val ws = WordSegmenter.seg(line)
    println(ws)

    val spark = SparkSession.builder()
      .master("local[2]").appName("jobClean").getOrCreate()
    spark.sparkContext.setLogLevel("WARN")

    val schema = StructType(Array("url","name","salary","province","city","exp","edu","num","pubtime","cname","ctype","ctrade","cnum","cate1","cate2","welfare","detail")
      .map(col=>StructField(col,DataTypes.StringType)))
    val data = spark.read.option("header","false").schema(schema).csv("result/cleaned/*")
/*    Java开发工程师
Web前端开发
数据分析师
UI设计师
 */

    val result = data.groupBy(col("cate1"),col("cate2")).agg(
      col("cate1"),col("cate2")
    )


    print(result.count())
//    所有分类
    val cate = "大数据开发工程师"
//    过滤指定数据
      val jobs = data.rdd
        .filter(row=>row.getAs[String]("cate2").equals(cate))
      println(jobs.count())
//    统计词频
      val words = jobs.flatMap(row=>{
        var detail = row.getAs[String]("detail")
        val result = WordCount2.word_split(detail)


//            var result = Array[String]()
//          if(detail!=null)
//            result = detail.split("\\|")


//        这里要不要去重先呢 distinct
            result.map(w=>(w,1))
//        println( result.mkString(","))
      }).reduceByKey(_+_).sortBy(i=>i._2,ascending = false).take(100)
words.foreach(println)


    spark.stop()
    System.exit(2)


  }
}
