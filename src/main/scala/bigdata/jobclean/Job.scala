package bigdata.jobclean

//job类的一个item
case class Job(
                val url:String,
                val name:String,//名字
                val salary:Int,//工资
                var province:String,//省份
                val city:String,//地级市
                val exp:String,//经验
                val edu:String,//学历
                val num:Int,//招聘人数
                val pubtime:String,//发布时间
                val cname:String,//公司名字
                val ctype:String,//公司类型
                val ctrade:String,//公司行业
                val cnum:String,//公司规模
                val cate1:String,//分类1
                val cate2:String,//分类2
                val welfare:String,//福利
                val detail:String///m

              ){
//  implicit def str2Int(s:String): Int = Integer.parseInt(s)
//  def this()
//    {
//      this()
//    }

}