DATE_NAME=`date "+%Y-%m-%d"`
#日志目录
LOG_DIR=/home/gg/scripts/logs
LOG_FILE=${LOG_DIR}/${DATE_NAME}.log

	if [ ! -d ${LOG_DIR} ] 
	then 
	echo "日志目录文件不存在，自动创建:" ${LOG_DIR}
	mkdir -p ${LOG_DIR}
	fi

echo "开始处理..."
#将标准输出重定向到logfile,标准错误重定向到标准输出
/home/gg/scripts/job.sh > ${LOG_FILE} 2>&1
echo "处理完成"
