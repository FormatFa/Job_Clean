USE job;
DESC job;
SELECT province,count(1) c FROM job GROUP BY province order by c desc;

# 城市
SELECT city,count(1) c FROM job GROUP BY city order by c desc;
SELECT cate2,count(1) c FROM job GROUP BY cate2 order by c desc;
# 经验
SELECT exp,count(1) c FROM job GROUP BY exp order by c desc;
#学历
SELECT edu,count(1) c FROM job GROUP BY edu order by c desc;
# 发布时间
SELECT pubtime,count(1) c FROM job GROUP BY pubtime order by c desc;
#数据源alter
SELECT site,count(1) c FROM job GROUP BY site order by c desc;
SELECT cnum,count(1) c FROM job GROUP BY cnum order by c desc;

#初中以下学历的是什么工作
SELECT name from job where edu="初中及以下";

select name,edu,site from job where cname="江苏苏美达机电有限公司";

# 大数据的
select cname,name  from job where name like "%大数据%";
select city,count(1) c from job where lower(name) like "%java%" group by city order by c desc ;
select city,count(1) c from job where name like "%Java%" group by city order by c desc ;

#各城市的平均薪资alter
select city, avg(salary)  s from job group by city order by s desc;

#各省
select province, avg(salary)  s from job  group by province order by s;
select province, avg(salary)  s from job where site="liepin" group by province order by s;

#职位名字
SELECT name,count(1) c FROM job GROUP BY name order by c desc;
#SELECT cname,count(1) c FROM job GROUP BY        

select edu,count(1) c from  job where cate2="数据分析师" group by edu order by c;
# 时间范围
select pubtime,count(1) from job group by pubtime order by pubtime;
# 不同分类个数
select cate1,cate2,count(1) from job group by cate1,cate2;

# 招聘人数大于10 得工作信息alter
 select cate1,cate2 , count(1)  c from job where num>9 group by cate1,cate2 order by c desc;
 select city , count(1)  c from job where num<=9 group by city order by c;
 select  count(1)  c from job where num>9 ;
 select cate2,name from job where num=10;
 
 # 测试其他类得描述
 select detail from job where cate2="其他";


## 词频测试